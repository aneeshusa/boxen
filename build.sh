#!/usr/bin/sh

# This file is part of Boxen.
#
# Boxen is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Boxen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Boxen.  If not, see <http://www.gnu.org/licenses/>.

set -e
set -u

main () {
    local box template_path vm_output_dir

    box="$1"
    # Store VM disk files hopefully on tmpfs
    # For speed and to avoid excess disk writes
    vm_output_dir=$(mktemp -d)
    template_path=$(nix-build --no-out-link -A "${box}");
    packer build  \
        -var "vm_output_dir=${vm_output_dir}" \
        -var "box_output_dir=boxes/${box}.box" \
        "${template_path}"
    rm -r "${vm_output_dir}"
}

if [ $# -ne 1 ]; then
    printf 'Must supply a box name to build.\n'
    exit 1
fi

if [ -d boxes ]; then
    printf 'The output boxes directory already exists.\n'
    printf 'Please remove it manually to avoid unintentional clobbering.\n'
    exit 1
fi

main "$@"
