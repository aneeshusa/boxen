# This file is part of Boxen.
#
# Boxen is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Boxen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Boxen.  If not, see <http://www.gnu.org/licenses/>.

with import <nixpkgs> {}; let

  year = "2016";
  month = "01";
  day =  "01";
  version = "${year}.${month}.${day}";
  archive_date = "${year}/${month}/${day}";

  archIso = fetchurl {
    url = "https://archive.archlinux.org/iso/${version}/archlinux-${version}-dual.iso";
    sha256 = "0a2s4bap60k25889pjv5k8k362zz8nim75rw0cj6bqahd6n12phq";
  };

  mkPackerTemplate = import ./nix/packer-template.nix;
in

{
  "arch-base" = writeText "arch-base-${version}-template.json" (builtins.toJSON (mkPackerTemplate {
    guest_os_type = "ArchLinux_64";
    iso_url = "${archIso}";
    boot_command = [
      # pacman -Sy is evil, but less evil than sh
      "pacman --sync --refresh --noconfirm python" "<enter>"
      "export ARCHIVE_DATE='${archive_date}'" "<enter>"
      "export TRANSIENT_ROOT_PASSWORD='{{user `transient_root_password`}}'" "<enter>"
      "wget \"http://{{ .HTTPIP }}:{{ .HTTPPort }}/arch-mirrorlist\" -O \"$HOME/arch-mirrorlist\"" "<enter>"
      "wget \"http://{{ .HTTPIP }}:{{ .HTTPPort }}/chroot-script.sh\" -O \"$HOME/chroot-script.sh\"" "<enter>"
      "curl \"http://{{ .HTTPIP }}:{{ .HTTPPort }}/arch-base.py\" | python3" "<enter>"
    ];
    shutdown_command = "systemctl poweroff";
  }));
}
