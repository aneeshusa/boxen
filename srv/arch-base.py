#!/usr/bin/env python3

# This file is part of Boxen.
#
# Boxen is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Boxen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Boxen.  If not, see <http://www.gnu.org/licenses/>.

import os
import subprocess
import shutil

def run(*args, **kwargs):
    kwargs['check'] = True
    return subprocess.run(*args, **kwargs)

def install():
    print("Checking environment variables.")
    print("Transient root password is {}.".format(
        os.environ['TRANSIENT_ROOT_PASSWORD']))
    print("Package archive date to use is {}.".format(
        os.environ['ARCHIVE_DATE']))

    mirror = 'Server=https://archive.archlinux.org/repos/{}/$repo/os/$arch'
    archive_date = os.environ['ARCHIVE_DATE']
    with open('/etc/pacman.d/mirrorlist', mode='w') as mirrorlist:
        mirrorlist.write(mirror.format(archive_date) + '\n')

    run(['sgdisk',
         '--clear',
         '--new=0:0:+512M', '--typecode=0:ef00',
         '--new=0:0:0',     '--typecode=0:8300',
         '/dev/sda'])
    run(['mkfs.fat', '-F32', '-n', 'boot', '/dev/sda1'])
    run(['mkfs.btrfs', '--label', 'root', '/dev/sda2'])

    run(['mount',
         '--options', 'noatime,autodefrag,compress=lzo,space_cache',
         '/dev/sda2', '/mnt'])
    os.mkdir('/mnt/boot')
    run(['mount',
         '--options', 'noatime',
         '/dev/sda1', '/mnt/boot'])

    pkgs = ['base',
            'btrfs-progs', 'openssh', 'sudo', 'virtualbox-guest-utils-nox']
    # Don't copy pinned mirrorlist to new install to allow for later updates
    run(['pacstrap', '-M', '/mnt'] + pkgs)

    with open('/mnt/etc/fstab', mode='w') as fstab:
        run(['genfstab', '-U', '-p', '/mnt'], stdout=fstab)

    run(['systemctl', 'disable', '--now', 'dhcpcd@'])
    run(['pacman',
         '--remove',
         '--cascade',
         '--nosave',
         '--recursive',
         '--unneeded',
         '--noconfirm',
         'netctl', 'dhcpcd'])

    real_mirrorlist_path = '{}/arch-mirrorlist'.format(os.environ['HOME'])
    shutil.move(real_mirrorlist_path, '/mnt/etc/pacman.d/mirrorlist')

    chroot_script_path = '{}/chroot-script.sh'.format(os.environ['HOME'])
    with open(chroot_script_path, 'r') as chroot_script:
        run(['/bin/arch-chroot', '/mnt'], stdin=chroot_script)
    os.remove(chroot_script_path)

    os.remove('/mnt/root/.bash_history')
    os.remove('/mnt/etc/resolv.conf')
    os.symlink('/run/systemd/resolve/resolv.conf', '/mnt/etc/resolv.conf')
    run(['umount', '--recursive', '/mnt'])

    print("Base provisioning complete. Shutting down.")
    run(['chpasswd'], input='root:{}'.format(
        os.environ['TRANSIENT_ROOT_PASSWORD']).encode('utf-8'))
    run(['systemctl', 'start', 'sshd'])

if __name__ == '__main__':
    install()
