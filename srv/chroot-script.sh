#!/usr/bin/env sh

# This file is part of Boxen.
#
# Boxen is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Boxen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Boxen.  If not, see <http://www.gnu.org/licenses/>.

set -e
set -u

pacman --remove --cascade --nosave --recursive --unneeded --noconfirm netctl dhcpcd jfsutils reiserfsprogs mdadm nano

printf 'en_US.UTF-8 UTF-8\n' > /etc/locale.gen
locale-gen
printf 'LANG=en_US.UTF-8\n' > /etc/locale.conf

hwclock --systohc --utc

bootctl install

cat <<EOF >/boot/loader/entries/arch.conf
title Arch Linux
linux /vmlinuz-linux
initrd /initramfs-linux.img
options root=/dev/sda2 rw
EOF

cat <<EOF >/boot/loader/loader.conf
default arch
timeout 3
EOF

cat <<EOF >/etc/systemd/network/ethernet-dhcp.network
[Match]
Name=e*

[Network]
DHCP=both
EOF

systemctl enable systemd-networkd.service systemd-resolved.service sshd.service

useradd -m "vagrant"
chpasswd <<EOF
vagrant:vagrant
EOF

mkdir -p "/home/vagrant/.ssh"
chown --recursive "vagrant" "/home/vagrant/"
chmod 700 "/home/vagrant/"
chmod 700 "/home/vagrant/.ssh/"
curl --location https://github.com/mitchellh/vagrant/raw/master/keys/vagrant.pub --output "/home/vagrant/.ssh/authorized_keys"
chmod 600 "/home/vagrant/.ssh/authorized_keys"
chown --recursive "vagrant:vagrant" "/home/vagrant/.ssh"

cat <<EOF >/etc/sudoers
Defaults env_reset
vagrant ALL=(ALL) NOPASSWD: ALL
EOF

usermod --password '!' root

cat <<EOF >/etc/modules-load.d/virtualbox-guest.conf
vboxguest
vboxsf
vboxvideo
EOF

pacman --sync --clean --clean --noconfirm
