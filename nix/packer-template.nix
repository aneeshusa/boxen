# This file is part of Boxen.
#
# Boxen is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Boxen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Boxen.  If not, see <http://www.gnu.org/licenses/>.

{ guest_os_type, iso_url, boot_command, shutdown_command }:

with import <nixpkgs> {};

let

  # To workaround https://github.com/mitchellh/packer/issues/2772
  transient_root_password = "just-for-ssh-to-poweroff";

in {
  variables = {
    vm_output_dir = null;
    box_output_dir = null;
    inherit transient_root_password;
  };
  builders = [{
    type = "virtualbox-iso";
    inherit guest_os_type iso_url;
    iso_checksum_type = "none"; # Nix already does checksum verification
    iso_interface = "sata";
    guest_additions_mode = "disable";
    vboxmanage = [[
      "modifyvm" "{{ .Name }}"
      "--hwvirtex" "on"
      "--nestedpaging" "on"
      "--largepages" "on"
      "--vtxvpid" "on"
      "--ioapic" "on"
      "--memory" "1024"
      "--cpus" "1"
      "--cpuhotplug" "on"
      "--nictype1" "virtio"
      "--firmware" "efi"
      "--rtcuseutc" "on"
      "--hpet" "on"
      "--audio" "none"
      "--usb" "off"
      "--usbehci" "off"
      "--usbxhci" "off"
    ]];
    hard_drive_interface = "sata";
    disk_size = 20000;
    http_directory = "srv";
    headless = true;
    boot_wait = "40s";
    inherit boot_command;
    ssh_username = "root";
    ssh_password = transient_root_password;
    ssh_wait_timeout = "3000s";
    inherit shutdown_command;
    # Packer requires a non-existing directory here
    output_directory = "{{user `vm_output_dir`}}/build";
  }];
  provisioners = [];
  post-processors = [{
    type = "vagrant";
    output = "{{user `box_output_dir`}}";
  }];
}
